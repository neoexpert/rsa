package com.neoexpert;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.math.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	//test des erweiterten euklidischen Algorithmus:
		System.out.println(EEA.eea(BigInteger.valueOf(         128),BigInteger.valueOf(34)));
		System.out.println(EEA.eeaIterative(BigInteger.valueOf(128),BigInteger.valueOf(34)));
		//RSA test:
		RSA rsa=new RSA();

		String s="Hello RSA!";
		BigInteger t=new BigInteger(s.getBytes());

		System.out.println("t="+t);
		BigInteger g=rsa.encrypt(t);
		System.out.println("g="+g);

		BigInteger t2=rsa.decrypt(g);
		System.out.println("t="+t2);
		System.out.println("s="+new String(t2.toByteArray()));
        assertTrue( true );
    }
}
