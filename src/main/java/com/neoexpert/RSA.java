package com.neoexpert;

import java.math.*;
import java.util.*;
import java.security.*;


/**
 * Hello world!
 *
 */
public class RSA
{
	public class Key{
		public Key(BigInteger ed,BigInteger N){
			this.ed=ed;
			this.N=N;
		}
		public BigInteger ed;
		public BigInteger N;
	}
	private Key priv_key;
	private Key pub_key;

	public RSA(Key priv_key,Key pub_key){
		this.priv_key=priv_key;
		this.pub_key=pub_key;
	}
	public RSA(){
		generateRandomKeys(1024);
	}
	public RSA(int keylength){
		generateRandomKeys(keylength);
	}
	private static BigInteger BONE=BigInteger.ONE;
	public void generateRandomKeys(int minkeylength)
	{
		Random rand=new SecureRandom();
		BigInteger p = new BigInteger(minkeylength+rand.nextInt(minkeylength), 1000000000, rand);
		System.out.println("p = "+p);
		//System.out.println("p is prime: "+isPrime(p));

		BigInteger q = new BigInteger(minkeylength+rand.nextInt(minkeylength), 1000000000, rand);
		System.out.println("q = "+q);
		//System.out.println("q is prime: "+isPrime(q));

		BigInteger N=p.multiply(q);
		System.out.println("N = p*q = "+N);
		BigInteger phiN=p.subtract(BONE).multiply(q.subtract(BONE));
		System.out.println("phi(N) = (p-1)*(q-1) = "+phiN);

		BigInteger r;
		do {
    		r = new BigInteger(phiN.bitLength(), rand);
		} while (r.compareTo(phiN) >= 0);
		BigInteger e=getE(r,phiN);
		System.out.println("e = "+e);
		System.out.println("ggt(e,phiN) = "+e.gcd(phiN));
		EEA.EEAResult res=EEA.eeaIterative(e,phiN);
		BigInteger d=res.y;
		System.out.println("d = "+d);
		priv_key=new Key(d,N);
		pub_key=new Key(e,N);
	}
	public BigInteger encrypt(BigInteger t){
		return t.modPow(pub_key.ed,pub_key.N);
	}
	public BigInteger decrypt(BigInteger t){
		return t.modPow(priv_key.ed,priv_key.N);
	}
	public BigInteger getE(BigInteger r,BigInteger phiN){
		if(r.gcd(phiN).equals(BONE))
			return r;
		return getE(r.add(BONE),phiN);
	
	}
	public BigInteger nextPrime(BigInteger bi){
		if(isPrime(bi))
			return bi;
		return nextPrime(bi.add(BigInteger.ONE));
	}
	private static BigInteger two=BigInteger.valueOf(2);
	private static BigInteger three=BigInteger.valueOf(3);
	public boolean isPrime(BigInteger n) {

		if (!n.isProbablePrime(10)) {
			return false;
		}

		if (n.compareTo(BigInteger.ONE) == 0 || n.compareTo(two) == 0) {
			return true;
		}
		BigInteger root = appxRoot(n);
		System.out.println("Using approximate root " + root);

		int cnt = 0;
		for (BigInteger i = three; i.compareTo(root) <= 0; i = i
				.nextProbablePrime()) {
			cnt++;
			if (cnt % 1000 == 0) {
				System.out.println(cnt + " Using next prime " + i);
			}
			if (n.mod(i).equals(BigInteger.ZERO)) {
				return false;
			}

				}
		return true;

	}

	private BigInteger appxRoot(final BigInteger n) {
		BigInteger half = n.shiftRight(1);
		while (half.multiply(half).compareTo(n) > 0) {
			half = half.shiftRight(1);
		}
		return half.shiftLeft(1);
	}
}
