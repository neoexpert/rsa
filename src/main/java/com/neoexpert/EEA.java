package com.neoexpert;

import java.math.*;
import java.util.*;


/**
 * Extended Euclidian Algorithm
 *
 */
public class EEA 
{
	public static class EEAResult{
		public EEAResult(BigInteger gcd, BigInteger x, BigInteger y){
			this.gcd=gcd;
			this.x=x;
			this.y=y;
		}
		public BigInteger gcd;
		public BigInteger x;
		public BigInteger y;
		@Override
		public String toString(){
			return "("+gcd+","+x+","+y+")";
		}
	}
	public static EEAResult eea(BigInteger a,BigInteger b)
	{
		if(a.compareTo(b)<0)
			return eea(b,a);
		if(b.equals(BigInteger.ZERO))return new EEAResult(a,BigInteger.ONE,BigInteger.ZERO);
		EEAResult r= eea(b,a.subtract(b.multiply(a.divide(b))));
		/*
		System.out.print("a="+a);
		System.out.print(",");
		System.out.print("b="+b);
		System.out.print(",");
		System.out.print("q="+(a.divide(b)));
		System.out.print(",");
		System.out.print("r="+(a.subtract(b.multiply(a.divide(b)))));
		System.out.print(",");
		System.out.print("x="+r.x);
		System.out.print(",");
		System.out.print("y="+r.y);
		System.out.println();

		System.out.println("###");
		System.out.println("r.y="+r.x+"-"+(a.divide(b))+"*"+r.y);
		System.out.println("###");
		*/
		BigInteger tmp=r.y;
		//r.y=r.x-(a/b)*r.y;
		r.y=r.x.subtract(a.divide(b).multiply(r.y));
		r.x=tmp;

		return r;
	}
	public static EEAResult eeaIterative(BigInteger a, BigInteger b)
	{
		BigInteger x = BigInteger.ZERO;
		BigInteger lastx = BigInteger.ONE;
		BigInteger y = BigInteger.ONE;
		BigInteger lasty = BigInteger.ZERO;
		while (!b.equals(BigInteger.ZERO))
		{
			BigInteger[] quotientAndRemainder = a.divideAndRemainder(b);
			BigInteger quotient = quotientAndRemainder[0];

			BigInteger temp = a;
			a = b;
			b = quotientAndRemainder[1];

			temp = x;
			x = lastx.subtract(quotient.multiply(x));
			lastx = temp;

			temp = y;
			y = lasty.subtract(quotient.multiply(y));
			lasty = temp;
		}

		EEAResult result = new EEAResult(a,lasty,lastx);
		return result;
	}
}
